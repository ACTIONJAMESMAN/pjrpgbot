package pjrpgbot

import kotlin.test.Test
import kotlin.test.assertTrue
import kotlin.test.assertEquals
import pjrpgbot.commands.Roll

class RollTest {

    // TODO figure out how to mock calls to Random.
    //      these tests have the potential to create false positives

    val config = Config(
        "token",
        100,
        mapOf(
            6 to mapOf(
                1 to "one",
                2 to "two",
                3 to "three",
                4 to "four",
                5 to "five",
                6 to "six",
            ),
        ),
        mapOf("maxDieCount" to "error message for maxDieCount")
    )


    @Test
    fun `On single mapped dice roll, return mapped string`() {
        val rollCommand = Roll(config)
        for (i in 0..24) {
            var actual = rollCommand.run(listOf("d6")).get(0)
            assertTrue(config.diceTextMap.get(6)!!.containsValue(actual))
        }
    }

    @Test
    fun `On single unmapped dice roll, return an unmapped string with dice identifier`() {
        val rollCommand = Roll(config)
        for (i in 0..12) {
            var (actualIdentifier, actualValue) = rollCommand.run(listOf("d3")).get(0).split(" ")
            assertEquals("d3:", actualIdentifier)
            assertTrue(actualValue.toInt() in 1..3)
        }
    }

    @Test
    fun `On single mapped multi-dice roll, return mapped string`() {
        val rollCommand = Roll(config)
        val actual = rollCommand.run(listOf("24d6")).get(0).split(" ")
        assertEquals(24, actual.size)
        actual.forEach {
            assertTrue(config.diceTextMap.get(6)!!.containsValue(it))
        }
    }

    @Test
    fun `On mutliple mapped dice rolls of different types, return mapped string`() {
        val rollCommand = Roll(config)
        val (actuald6, actuald3) = rollCommand.run(listOf("24d6", "d3"))
        assertEquals(24, actuald6.split(" ").size)
        actuald6.split(" ").forEach {
            assertTrue(config.diceTextMap.get(6)!!.containsValue(it))
        }
        val (actuald3Identifier, actuald3Value) = actuald3.split(" ")
        assertEquals("d3:", actuald3Identifier)
        assertTrue(actuald3Value.toInt() in 1..3)
    }

    @Test
    fun `On single mapped multi-dice roll over limit, return limit of rolls and error`() {
        val rollCommand = Roll(config)
        val (actualRolls, errorMessage) = rollCommand.run(listOf((config.maxDieCount + 1).toString() + "d6"))
        assertEquals(config.maxDieCount, actualRolls.split(" ").size)
        assertEquals(errorMessage, config.errors.get("maxDieCount"))
    }

    @Test
    fun `On multiple mapped dice rolls totaling over limit, return limit of rolls and error`() {
        val rollCommand = Roll(config)
        val expectedd6Count = config.maxDieCount / 2;
        val expectedd3Count = config.maxDieCount - expectedd6Count;
        val (actuald6, actuald3, errorMessage) =
            rollCommand.run(listOf(
                                expectedd6Count.toString() + "d6",
                                config.maxDieCount.toString() + "d3"
            ))
        assertEquals(expectedd6Count, actuald6.split(" ").size)
        actuald6.split(" ").forEach {
            assertTrue(config.diceTextMap.get(6)!!.containsValue(it))
        }
        val actuald3Identifier = actuald3.split(" ").get(0)
        val actuald3Values = actuald3.split(" ").drop(1)
        assertEquals("d3:", actuald3Identifier)
        assertEquals(expectedd3Count, actuald3Values.size)
        actuald3Values.forEach {
            assertTrue(it.toInt() in 1..3)
        }
        assertEquals(errorMessage, config.errors.get("maxDieCount"))
    }
}
