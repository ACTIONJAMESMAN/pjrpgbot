package pjrpgbot

import kotlin.test.Test
import kotlin.test.assertEquals
import org.mockito.Mockito
import pjrpgbot.commands.*
import pjrpgbot.CommandController

class ControllerTest {

    @Test
    fun `On call to mocked command, return successful response`() {
        val expected = listOf("expected")
        val mockRollCommand = Mockito.mock(Roll::class.java)
        Mockito.`when`(mockRollCommand.run(listOf("argument"))).thenReturn(expected)
        Mockito.`when`(mockRollCommand.name).thenReturn("command")

        val commandController = CommandController(listOf(mockRollCommand))

        val actual = commandController.run("command argument")

        Mockito.verify(mockRollCommand).run(listOf("argument"))
        assertEquals(expected, actual)
    }
}
