package pjrpgbot

import pjrpgbot.commands.*
import kotlin.reflect.*

class CommandController(var commands: List<Command> = listOf(
                            Roll(),
)) {
    val commandMap: Map<String, KFunction1<List<String>, List<String>>>
        = commands.map { it.name to it::run }.toMap()

    public fun run(commandMessage: String): List<String>? {
        val commandArgs: List<String> = commandMessage.split(" ")

        return commandMap.get(commandArgs.get(0))?.invoke(commandArgs.drop(1))
    }
}
