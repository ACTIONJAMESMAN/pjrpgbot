package pjrpgbot

import com.sksamuel.hoplite.*

data class Config(
    val botToken: String,
    val maxDieCount: Int,
    val diceTextMap: Map<Int, Map<Int, String>>,
    val errors: Map<String, String>,
) {
    companion object {
        private var INSTANCE: Config? = null

        fun getInstance(fileName: String = "/config.yaml"): Config = INSTANCE ?: synchronized(this) {
            ConfigLoader().loadConfigOrThrow<Config>(fileName).also { INSTANCE = it }
        }
    }
}
