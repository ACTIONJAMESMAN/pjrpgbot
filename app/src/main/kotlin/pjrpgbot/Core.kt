package pjrpgbot

import dev.kord.core.*
import dev.kord.core.event.message.*
import dev.kord.core.entity.*
import dev.kord.core.cache.data.*

suspend fun main() {
    val commandController = CommandController()
    val kord = Kord(Config.getInstance().botToken)

    kord.on<MessageCreateEvent> {

        if (message.author?.id == kord.selfId) return@on

        if (!message.content.startsWith("!")) return@on

        commandController.run(message.content.drop(1))?.forEach {
            message.channel.createMessage(it)
        }
    }

    kord.login()
}
