package pjrpgbot.commands

import kotlin.random.*
import pjrpgbot.Config

class Roll(val config: Config = Config.getInstance()) : Command {
    override val name:String = "roll"

    override fun run(args: List<String>): List<String> {
        var sendMessages: MutableList<String> = mutableListOf<String>()

        var totalDieCount: Int = 0
        var totalDieCountPassed: Boolean = false

        args.forEach {
            val dieInfo: List<String> = it.split("d")
            val dieType: Int = dieInfo.elementAt(1).toInt()
            val dieCount: Int = dieInfo.elementAtOrNull(0)?.toIntOrNull() ?: 1

            var rolledDiceMessage: String = ""
            var mappingsNotFound: Boolean = false

            for (i in (totalDieCount + 1)..((totalDieCount + dieCount).coerceIn(0..config.maxDieCount))) {
                val rolledDieValue: Int = Random.nextInt(1, dieType + 1)
                config.diceTextMap.get(dieType)?.get(rolledDieValue)?.let {
                    rolledDiceMessage += it.toString() + " "
                } ?: run {
                    mappingsNotFound = true
                    rolledDiceMessage += rolledDieValue.toString() + " "
                }
            }
            if (!rolledDiceMessage.isNullOrEmpty()) {
                sendMessages.add(((if (mappingsNotFound) ("d" + dieType + ": ") else "") + rolledDiceMessage).trim(' '))
            }
            totalDieCount += dieCount
            if (totalDieCount > config.maxDieCount) {
                totalDieCountPassed = true
                return@forEach
            }
        }
        var errorMessage = ""
        if (totalDieCountPassed) {
            errorMessage += config.errors.get("maxDieCount")
        }
        if (!errorMessage.isNullOrEmpty()) {
            sendMessages.add(errorMessage)
        }

        return sendMessages;
    }
}
