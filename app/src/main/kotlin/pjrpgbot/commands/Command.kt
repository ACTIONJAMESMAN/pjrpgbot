package pjrpgbot.commands

import dev.kord.core.behavior.channel.*

interface Command {
    val name: String

    fun run(args: List<String>): List<String>
}
